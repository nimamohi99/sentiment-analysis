# Sentiment Analysis

A web interface that displays the emotion of an individual by analyzing textual data using natural language processing algorithms.